//============================================================================
// Name        : main.cpp
// Author      : YourNameHere
// Version     : 1
// Copyright   : 2012
// Description : Exceptions in C++, Ansi-style
//============================================================================
/*
Task 4: 
1. if someone will add null Person, remove or get Person witch doesen't exist.
2. before using getNext() function there is no check if there exist pointer to next list element.
   So even if there is empty list it not properlly use print function
*/

#include <iostream>
using namespace std;

#include "PersonValidation.h"
#include "ProgrammerException.h"
#include "ListExceptions.h"
#include "Person.h"
#include "List.h"

int main() {

	List* myList = new List("myList");

	//Add some persons
	try{		
		myList->add(0);
		myList->add(new Person("Jan","Kowalski","0127891"));
		myList->add(new Person("Pawel","Nowak","000000000"));
		myList->add(new Person("Adrian","Koder","000000000"));
	}
	catch(ProgrammerException & e1){
		cout << e1.what() << endl;
	}
	catch(NoNullPersonException & e)
	{
		cout << e.what() << endl;
	}
	

	myList->print();

	//remove single person
	try{
		myList->remove(5);
	} catch(exception & e){
		cout << e.what() << endl;
	}

	myList->print();

	//get single person
	try{
		Person* p = myList->get(5);
		cout << p->getName() << std::endl<< std::endl;
	} catch(exception & e){
		cout << e.what() << endl;
	}

	//clean list
	myList->clean();
	myList->print();

	return 0;
}

/*
#include <iostream>
using namespace std;


class DivideByZeroException: public exception{
	virtual const char* what() const throw(){
		return "Can't divide by zero";
	}
} divideByZeroException;

int main(){
	int a,b,c;

	cout << "Type a and b numbers:";
	cin >> a >> b;



	try{
		if(b == 0) throw  divideByZeroException;
		
		c = a/b; 
		cout << endl << "c=a/b: " << c << endl;
	}
	catch(exception & e)
	{
		//Task 2 answer: not it will not create infinite recursion.
		//It created 2 first chance exceptions and one 
		//unhandled because this statement is not catched.
		throw divideByZeroException;
		cout << e.what();
	}

	return 0;
}
*/