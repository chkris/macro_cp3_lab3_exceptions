#include "Person.h"
#include "PersonValidation.h"
#include "ProgrammerException.h"

Person::Person(std::string name, std::string surname, std::string pesel): name(name), surname(surname), pesel(pesel) {
	if(PersonValidation().validate(this) == false) throw ProgrammerException("7", "Person.cpp", "Name, surname or pesel is invalid"); 
}

Person::~Person() {
}

