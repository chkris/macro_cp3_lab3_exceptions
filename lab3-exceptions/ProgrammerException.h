#ifndef PROGRAMMEREXCEPTION_H_
#define PROGRAMMEREXCEPTION_H_
#include<iostream>
#include <string>
#include<exception>
using namespace std;

class ProgrammerException: public exception{

	private:
		string codeLine;
		string messageClass;
		string message;
	public:
		ProgrammerException(string codeLine, string messageClass, string message): codeLine(codeLine), messageClass(messageClass), message(message){}
		
		virtual const char* what() const throw(){
			cout << "[ " + messageClass + ":" + codeLine + " ]: " + message; 
			return "";
		}
};

#endif