#include<iostream>
#include<exception>
using namespace std;

class NoNullPersonException : public exception {
	
public:
	NoNullPersonException(){}

	virtual const char* what() const throw(){
		return "Can't add null to Person list";
	}
};

class RemoveNullPersonException : public exception {
	
	virtual const char* what() const throw(){
		return "Can't remove null Person from list";
	}
};

class GetNullPersonException : public exception {
	
	virtual const char* what() const throw(){
		return "Can't get null to Person list";
	}
};
