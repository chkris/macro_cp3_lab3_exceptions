#include "ListItem.h"
#include <exception>
#include <iostream>
#include "ProgrammerException.h"

ListItem::ListItem(Person* person) try : person(person), next(0){
}
catch(...){
	throw;
}


ListItem::~ListItem() {
}


void ListItem::print() {
	Person* person = this->getPerson();
	std::cout << person->getName() << " " << person->getSurname() << "\t PESEL: " << person->getPesel() << std::endl;
}

