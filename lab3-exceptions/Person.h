#ifndef PERSON_H_
#define PERSON_H_

#include <string>

class Person {
private:
	std::string name;
	std::string surname;
	std::string pesel;

public:
	Person(std::string name, std::string surname, std::string pesel);
	virtual ~Person();

	const std::string& getName() const {
		return name;
	}

	void setName(const std::string& name) {
		this->name = name;
	}

	const std::string& getPesel() const {
		return pesel;
	}

	void setPesel(const std::string& pesel) {
		this->pesel = pesel;
	}

	const std::string& getSurname() const {
		return surname;
	}

	void setSurname(const std::string& surname) {
		this->surname = surname;
	}
};

#endif /* PERSON_H_ */
