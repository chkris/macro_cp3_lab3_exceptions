#include "Person.h"
#include <string>
#include<exception>
using namespace std;

class PersonValidation{
	public:
		static bool validate(Person * person){
			return notEmpty(person->getName()) && notEmpty(person->getSurname()) && peselIsElevenDigit(person->getPesel());
		}

	public:
		static bool notEmpty(string input){
			return !input.empty();
		}

		static bool peselIsElevenDigit(string pesel){
			if(pesel.size() == 11) return true;
			return false;
		}
};